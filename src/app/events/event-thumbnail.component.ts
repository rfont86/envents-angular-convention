import { Component, Input } from '@angular/core'
import { IEvent } from './shared/index'

@Component({
  selector: 'event-thumbnail',
  template: `
    <div [routerLink]="['/events', event.id]" class="well hoverwell thumbnail">
      <h2>{{event.name | uppercase}}</h2>
      <div>Date: {{event.date}}</div>
      <div>Time: {{event.time}}</div>
      <div>Price: {{event.price | currency:'USD'}}</div>
      <div>
        <span>Location: {{event.location.address}}</span>
        <span>{{event.location.city}}, {{event.location.country}}</span>
      </div>
    </div>
  `,
  styles: [`
    .thumbnail {min-height: 250px;}
    .pad-left {margin-left: 10px;}
    .well div {color; #bbb;}
  `]
})

export class EventThumbnailComponent {
  @Input() event:IEvent
}